<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/csvimport?lang_cible=km
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'អភិបាល ពួកតារាង',
	'ajouter_donnees' => 'បន្ថែមទិន្នន័យ',
	'ajouter_table' => 'បន្ថែម ទៅតារាង',
	'aucune_donnee' => 'គ្មានទិន្នន័យ ក្នុងឯកសារ។',

	// C
	'champs_table' => 'វាល នៃតារាង', # MODIF
	'csvimport' => 'នាំចូល CSV',

	// E
	'export_format' => 'ទ្រង់ទ្រាយ នៃឯកសារ ៖',
	'export_table' => 'ការនាំចេញ នៃតារាង៖ @table@',
	'exporter' => 'នាំចេញ',

	// F
	'fichier_absent' => 'ឯកសារអវត្តមាន',
	'fichier_choisir' => 'ឯកសារ CSV ត្រូវនាំចូល',
	'fichier_vide' => 'ឯកសារទទេ',

	// I
	'import_csv' => 'នាំចូល CSV ៖ @table@', # MODIF
	'import_export_tables' => 'នាំចូល / នាំចេញ ក្នុងតារាង',

	// L
	'lignes_totales' => 'សរុប @nb@ បន្ទាត់។',
	'lignes_totales_csv' => 'សរុប @nb@ បន្ទាត់ ក្នុងឯកសារ CSV។',

	// P
	'pas_importer' => 'គ្មានត្រូវនាំចូល',
	'premieres_lignes' => '@nb@ បន្ទាត់ដំបូង នៃឯកសារ។',
	'probleme_chargement_fichier' => 'បញ្ហា ពេលទាញយកឯកសារ',
	'probleme_chargement_fichier_erreur' => 'បញ្ហា ពេលទាញយកឯកសារ (កំហុស @erreur@)។',

	// T
	'table_vide' => 'តារាង "@table@" ទទេ។',
	'tout_remplacer' => 'ជំនួសទាំងអស់',

	// Z
	'z' => 'zzz'
);
