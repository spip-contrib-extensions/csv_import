<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/csvimport?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Beheer van tabellen',
	'ajouter_donnees' => 'Gegevens toevoegen',
	'ajouter_table' => 'Aan tabel toevoegen',
	'aucune_donnee' => 'Het bestand bevat geen gegevens.',
	'aucune_table_declaree' => 'Voor de CSV-import is geen tabel aangegeven',
	'avertissement_ajout' => 'De gegevens in het CSV-bestand worden toegevoegd aan tabel "@table@", zoals hieronder weergegeven.',
	'avertissement_remplacement' => 'Deze handeling gaat alle gegevens in de tabel wissen. De gegevns uit het bestand worden alsvolgt toegevoegd:',

	// C
	'caracteres_separation' => 'Scheidingsteken',
	'champs_csv' => 'Velden van het CSV-bestand',
	'champs_table' => 'Velden van de tabel: "gelabeld (key)"',
	'confirmation_ajout_base' => 'De CSV is correct aan de database toegevoegd!',
	'correspondance_incomplete' => 'Correspondenties CSV-Tabel onvolledig',
	'correspondance_indefinie' => 'Correspondenties CSV-Tabel niet gedefinieerd',
	'csvimport' => 'CSV Import',

	// D
	'delimiteur_indefini' => 'Delimiter niet bepaald',
	'description_table_introuvable' => 'Omschrijving van de tabel kan niet worden gevonden',

	// E
	'erreurs_ajout_base' => 'Er waren @nb@ fouten tijdens het toevoegen aan de database.',
	'etape' => '(Stap @step@ van 3)',
	'export_classique' => 'Klassieke CSV (,)',
	'export_excel' => 'CSV voor Excel ( ;)',
	'export_format' => 'Bestandsformaat:',
	'export_table' => 'Export van tabel: @table@',
	'export_tabulation' => 'CSV met tabs',
	'exporter' => 'Exporteren',
	'extrait_CSV_importe' => 'Extract van geïmporteerd CSV-bestand: ',
	'extrait_table' => 'Extract van tabel "@nom_table@": ',

	// F
	'fichier_absent' => 'Bestand niet gevonden',
	'fichier_choisir' => 'Te importeren CSV-bestand',
	'fichier_vide' => 'Leeg bestand',

	// I
	'import_csv' => 'CSV Import: "@table@"',
	'import_export_tables' => 'Tabellen importeren/exporteren',

	// L
	'ligne_entete' => '1<sup>e</sup> kopregel',
	'lignes_table' => 'Er staan @nb_resultats@ regels in tabel "@table@".',
	'lignes_totales' => '@nb@ regels in totaal.',
	'lignes_totales_csv' => '@nb@ regels in totaal in het CSV-bestand.',

	// N
	'nb_enregistrements' => '@nb@ registraties',
	'noms_colonnes_CSV' => 'Kolomnamen (verwacht) in het te importeren CSV-bestand: ',

	// P
	'pas_importer' => 'Niet importeren',
	'premieres_lignes' => '@nb@ kopregels van bestand.',
	'previsualisation_CSV_integre' => 'Vertoning van de eerste @nb@ regels van het CSV-bestand: ',
	'probleme_chargement_fichier' => 'Probleem bij het laden van het bestand',
	'probleme_chargement_fichier_erreur' => 'Probleem bij het laden van het bestand (fout @erreur@).',
	'probleme_inextricable' => 'Onoverkomelijk probleem...',

	// R
	'remplacer_toute_table' => 'Vervang de hele tabel',

	// T
	'table_vide' => 'Tabel "@table@" is leeg.',
	'tables_declarees' => 'Gedeclareerde tabellen',
	'tables_presentes' => 'Tabellen in de database',
	'tout_remplacer' => 'Alles vervangen',

	// Z
	'z' => 'zzz'
);
