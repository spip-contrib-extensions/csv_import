<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/csvimport?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Administrar las tablas',
	'ajouter_donnees' => 'Añadir los datos',
	'ajouter_table' => 'Añadir a la tabla',
	'aucune_donnee' => 'El archivo no contiene ningún dato',
	'aucune_table_declaree' => 'Ninguna tabla declarada para la importación CSV',
	'avertissement_ajout' => 'Los datos del archivo CSV serán añadidos en la tabla "@table@" como ilustrado a continuación.',
	'avertissement_remplacement' => 'Esta operación entrenará la supresión de todos los datos presentes en la tabla. Los datos del archivo serán insertados como presentado a continuación:',

	// C
	'caracteres_separation' => 'Carácter de separación',
	'champs_csv' => 'Campos del archivo CSV',
	'champs_table' => 'Campos de la tabla: "nombre (clave)"',
	'confirmation_ajout_base' => '¡El CSV fue importado con éxito en la base!',
	'correspondance_incomplete' => 'Correspondencias CSV-Tabla incompletas',
	'correspondance_indefinie' => 'Correspondencias CSV-Table indefinidas',
	'csvimport' => 'Importación CSV',

	// D
	'delimiteur_indefini' => 'Separador no definido',
	'description_table_introuvable' => 'No se encuentra la descripción de la tabla',

	// E
	'erreurs_ajout_base' => '@nb@ errores durante la importación en la base.',
	'etape' => '(Etapa @step@ de 3)',
	'export_classique' => 'CSV clásico (,)',
	'export_excel' => 'CSV para Excel ( ;)',
	'export_format' => 'Formato del archivo:',
	'export_table' => 'Exportación de la tabla: @table@',
	'export_tabulation' => 'CSV con tabulaciones',
	'exporter' => 'Exportar',
	'extrait_CSV_importe' => 'Extracto del archivo CSV importado: ',
	'extrait_table' => 'Extracto de la tabla "@nom_table@": ',

	// F
	'fichier_absent' => 'Archivo ausente',
	'fichier_choisir' => 'Archivo CSV a importar',
	'fichier_vide' => 'Archivo vacío',

	// I
	'import_csv' => 'Importación CSV: "@table@"',
	'import_export_tables' => 'Importación/Exportación en las tablas',

	// L
	'ligne_entete' => '1<sup>ra</sup> línea de encabezado',
	'lignes_table' => '@nb_resultats@ líneas en la tabla "@table@".',
	'lignes_totales' => '@nb@ líneas en total.',
	'lignes_totales_csv' => '@nb@ líneas en total en el archivo CSV.',

	// N
	'nb_enregistrements' => '@nb@ registros',
	'noms_colonnes_CSV' => 'Nombres de columnas (esperados) del archivo CSV a importar: ',

	// P
	'pas_importer' => 'No importar',
	'premieres_lignes' => '@nb@ primeras líneas del archivo.',
	'previsualisation_CSV_integre' => 'Vista previa de las @nb@ primeras líneas del archivo CSV integradas en la tabla: ',
	'probleme_chargement_fichier' => 'Problema al momento de cargar el archivo',
	'probleme_chargement_fichier_erreur' => 'Problema al momento de cargar el archivo (error @erreur@).',
	'probleme_inextricable' => 'Problema inextricable...',

	// R
	'remplacer_toute_table' => 'Remplazar toda la tabla',

	// T
	'table_vide' => 'La tabla "@table@" esta vacía.',
	'tables_declarees' => 'Tablas declaradas',
	'tables_presentes' => 'Tablas presentes en la base',
	'tout_remplacer' => 'Remplazar todo',

	// Z
	'z' => 'zzz'
);
