<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/csvimport?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'administrer_tables' => 'Управление таблицами',
	'ajouter_donnees' => 'Добавить данные',
	'ajouter_table' => 'Добавить в таблицу',
	'aucune_donnee' => 'В файле нет данных.',
	'aucune_table_declaree' => 'Нет таблиц, заявленных для CSV импорта',
	'avertissement_ajout' => 'Данные из CSV-файла будут добавлены в таблицу «@table@» , как показано выше.',
	'avertissement_remplacement' => 'Эта операция приведет к удалению всех данных в таблице. Файл данных будет вставлен следующим образом:',

	// C
	'caracteres_separation' => 'Знак-разделитель',
	'champs_csv' => 'Поля в CSV-файле',
	'champs_table' => 'Поле в таблице: «метка (ключ)»',
	'confirmation_ajout_base' => 'CSV-файл успешно добавлен в базу данных!',
	'correspondance_incomplete' => 'Соответствие CSV-Таблица неполно',
	'correspondance_indefinie' => 'Соответствия CSV-Таблица не определены',
	'csvimport' => 'CSV-импорт',

	// D
	'delimiteur_indefini' => 'Разделитель не определён',
	'description_table_introuvable' => 'Описание таблицы не найдено',

	// E
	'erreurs_ajout_base' => 'Произошло @nb@ ошибок вставки в базу данных.',
	'etape' => '(Шаг @step@ из 3)',
	'export_classique' => 'стандартный CSV (,)',
	'export_excel' => 'CSV для Excel ( ;)',
	'export_format' => 'Формат файла:',
	'export_table' => 'Экспорт таблицы: @table@',
	'export_tabulation' => 'CSV с разделителями табуляцией',
	'exporter' => 'Экспорт',
	'extrait_CSV_importe' => 'Фрагмент CSV-файла для импорта: ',
	'extrait_table' => 'Фрагмент из таблицы «@nom_table@»: ',

	// F
	'fichier_absent' => 'Отсутствует файл',
	'fichier_choisir' => 'CSV-файл для импорта',
	'fichier_vide' => 'Пустой файл',

	// I
	'import_csv' => 'CSV-импорт: «@table@»',
	'import_export_tables' => 'Импорт / Экспорт для таблиц',

	// L
	'ligne_entete' => '1<sup>я</sup> строка - заголовки',
	'lignes_table' => 'Всего @nb_resultats@ строк в таблице «@table@».',
	'lignes_totales' => '@nb@ строк всего.',
	'lignes_totales_csv' => '@nb@ строк всего в CSV-файле.',

	// N
	'nb_enregistrements' => '@nb@ записей сохранено',
	'noms_colonnes_CSV' => 'Имена столбцов (ожидаемые) CSV-файла для импорта:',

	// P
	'pas_importer' => 'Не импортировано',
	'premieres_lignes' => '@nb@ первые строки в файле',
	'previsualisation_CSV_integre' => 'Предварительный просмотр первых @nb@ строк CSV-файла, импортируемого в таблицу: ',
	'probleme_chargement_fichier' => 'Проблема при загрузке файла',
	'probleme_chargement_fichier_erreur' => 'Проблемы с загрузкой файла (ошибка @erreur@).',
	'probleme_inextricable' => 'Нераспознанная ошибка...',

	// R
	'remplacer_toute_table' => 'Заменить всю таблицу',

	// T
	'table_vide' => 'Таблица «@table@» пуста.',
	'tables_declarees' => 'Объявленные таблицы',
	'tables_presentes' => 'Таблицы, имеющиеся в базе данных',
	'tout_remplacer' => 'Заменить всё',

	// Z
	'z' => 'zzz'
);
